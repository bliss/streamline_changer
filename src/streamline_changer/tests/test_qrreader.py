def test_clear_buffer(sr700nl20):
    assert sr700nl20.clearBuffer() is None


def test_read(sr700nl20):
    assert sr700nl20.read() == "mysample0"
