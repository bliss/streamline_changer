import pytest
from streamline_changer.sample_changer import SampleChanger
from streamline_changer.qrreader import SR700NL20


class MockAxis:
    def __init__(self) -> None:
        self.position = 0

    def move(self, position, **_):
        self.position = position

    def wait_move(self, **_):
        pass


@pytest.fixture()
def mock_axis():
    return MockAxis()


@pytest.fixture()
def mock_sr700nl20(mocker, mock_axis):
    mock_socket = mocker.patch("socket.socket")

    request = b""

    def sendall(data):
        nonlocal request
        request = data

    def recv(nbytes):
        nonlocal request
        if not request:
            response = ""  # TODO: some error code?
        elif request.startswith(b"%BCLR"):
            response = ""
        elif request.startswith(b"LON"):
            response = f"mysample{mock_axis.position}"
        elif request.startswith(b"WB"):
            response = "TODO"
        elif request.startswith(b"SAVE"):
            response = "TODO"
        else:
            raise NotImplementedError(f"response to {request} not mocked")
        return (response + "\r").encode()

    mock_socket.return_value.sendall.side_effect = sendall
    mock_socket.return_value.recv.side_effect = recv


@pytest.fixture()
def sr700nl20(mock_sr700nl20):
    return SR700NL20("localhost", 9004)


@pytest.fixture()
def sample_changer(mock_sr700nl20, mock_axis):
    return SampleChanger(mock_axis, None)


@pytest.fixture()
def sample_changer_no_device(mock_axis):
    return SampleChanger(mock_axis, None)
