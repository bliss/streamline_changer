import pytest


def test_iterate_samples_without_qr(sample_changer, mock_axis):
    responses = list()
    for position, response in enumerate(sample_changer.iterate_samples_without_qr()):
        assert mock_axis.position == position
        responses.append(response)
    expected = [f"streamline_sample{i:02d}" for i in range(16)]
    assert responses == expected

    responses = list()
    indices = [0, 5, 3, 9]
    for position, response in zip(
        indices, sample_changer.iterate_samples_without_qr(indices)
    ):
        assert mock_axis.position == position
        responses.append(response)
    expected = [f"streamline_sample{i:02d}" for i in [0, 5, 3, 9]]
    assert responses == expected

    with pytest.raises(ValueError):
        response = list(sample_changer.iterate_samples_without_qr([20]))


def test_iterate_samples_with_qr(sample_changer, mock_axis):
    responses = list()
    for position, response in enumerate(sample_changer.iterate_samples()):
        assert mock_axis.position == position
        responses.append(response)
    expected = [f"mysample{i}" for i in range(16)]
    assert responses == expected

    responses = list()
    indices = [0, 5, 3, 9]
    for position, response in zip(indices, sample_changer.iterate_samples(indices)):
        assert mock_axis.position == position
        responses.append(response)
    expected = [f"mysample{i}" for i in [0, 5, 3, 9]]
    assert responses == expected

    with pytest.raises(ValueError):
        response = list(sample_changer.iterate_samples([20]))


def test_select_sample_without_qr(sample_changer, mock_axis):
    response = sample_changer.select_sample_without_qr(10)
    assert mock_axis.position == 10
    assert response == "streamline_sample10"

    with pytest.raises(ValueError):
        sample_changer.select_sample_without_qr(20)


def test_select_sample_with_qr(sample_changer, mock_axis):
    response = sample_changer.select_sample(10)
    assert mock_axis.position == 10
    assert response == "mysample10"

    with pytest.raises(ValueError):
        sample_changer.select_sample_without_qr(20)


def test_select_sample_no_device(sample_changer_no_device):
    err_msg = r"SR700NL20\('localhost', 9004\): cannot connect"
    with pytest.raises(ConnectionError, match=err_msg):
        sample_changer_no_device.select_sample(10)
