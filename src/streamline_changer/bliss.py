from inspect import signature
from streamline_changer.sample_changer import SampleChanger


class BlissSampleChanger(SampleChanger):
    def __init__(self, name, config):
        kwargs = dict()
        sig = signature(SampleChanger.__init__)
        parameters = set(sig.parameters) - {"self", "axis", "wago"}
        for parameter in parameters:
            value = config.get(parameter, None)
            if value is not None:
                kwargs[parameter] = value
        self.name = name
        super().__init__(config["sc_axis"], config["sc_wago"], **kwargs)
