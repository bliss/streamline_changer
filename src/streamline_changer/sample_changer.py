import time
from numbers import Integral, Number
from typing import Iterator, Optional, Tuple, Any
from streamline_changer.qrreader import SR700NL20


class SampleChanger:
    """Controls the sample changer assembly developed for STREAMLINE"""

    def __init__(
        self,
        translation,
        wago,
        qrreader_host: str = "localhost",
        qrreader_port: int = 9004,
        wait_to_stop_before_qr_code_reading: bool = True,
        step_size: Number = 1,
        initial_offset: Number = 0,
        delay_for_qr_code_reading: Number = 0.05,
        number_of_samples: Integral = 16,
    ):
        """
        :param translation: Bliss axis object
        :param wago: Bliss wago object
        :param qrreader_host:
        :param qrreader_port:
        :param wait_to_stop_before_qr_code_reading: read QR code when the holder stopped moving
        :param step_size: distance between two adjacent sample in axis user units
        :param initial_offset: axis position of the first sample
        :param delay_for_qr_code_reading: delay when reading the QR code while the holder is moving
        :param number_of_samples: number of sample in one holder
        """
        self._wago = wago
        self._translation = translation
        self._qrreader_host = qrreader_host
        self._qrreader_port = qrreader_port
        self._wait_to_stop_before_qr_code_reading = wait_to_stop_before_qr_code_reading
        self._step_size = step_size
        self._initial_offset = initial_offset
        self._number_of_samples = number_of_samples
        self._delay_for_qr_code_reading = delay_for_qr_code_reading
        self._allowed_sample_indices = tuple(range(number_of_samples))
        self.__qrreader = None
        self._delayCylinderLoading = 3
        self._delayCylinderRewind = 3
        self._maxAttempsToCheckHoming = 10
        self._maxspeed = 63
        self._gentlespeed = 5
        self._qrread_timeout = 5

        mot = self.translation
        start = self.sample_index_to_position(0)
        stop = self.sample_index_to_position(number_of_samples - 1)
        intervals = number_of_samples - 1
        self._ascan_arguments = mot, start, stop, intervals

    @property
    def translation(self):
        return self._translation

    @property
    def wago(self):
        return self._wago

    @property
    def qrreader(self):
        if self.__qrreader is None:
            self.__qrreader = SR700NL20(self._qrreader_host, self._qrreader_port)
        return self.__qrreader

    @property
    def current_position(self) -> float:
        return self.translation.position

    @property
    def current_sample_index(self) -> int:
        return self.position_to_sample_index(self.current_position)

    def sample_index_to_position(self, sample_index: int) -> float:
        return self._initial_offset + self._step_size * sample_index

    def position_to_sample_index(self, position: float) -> int:
        return int((position - self._initial_offset) / self._step_size)

    def ascan_arguments(self) -> Tuple[Any, float, float, int]:
        return self._ascan_arguments

    def iterate_samples(
        self, sample_indices: Optional[Tuple[int]] = None, **qrread_options
    ) -> Iterator[str]:
        sample_indices = self._parse_sample_indices(sample_indices)
        previous_index = self.current_sample_index
        for sample_index in sample_indices:
            # QR code reading is only allowed when moving to an adjacent sample
            if abs(previous_index - sample_index) > 1:
                wait_to_stop = True
            else:
                wait_to_stop = self._wait_to_stop_before_qr_code_reading
            previous_index = sample_index

            # Move to the requested sample
            # self.translation.offset = 0 # <=YW Removed this
            self.translation.move(
                self.sample_index_to_position(sample_index), wait=wait_to_stop
            )

            # Sleep to avoid reading the previous QR code when still moving
            if not wait_to_stop:
                time.sleep(self._delay_for_qr_code_reading)

            # Read QR code of the sample
            try:
                qr_response = self.qr_read(**qrread_options)
            except RuntimeError:
                qr_response = None

            # Wait for sample to stop
            self.translation.wait_move()

            # Read QR code when the read during motion failed
            if qr_response is None:
                qr_response = self.qr_read(**qrread_options)

            yield qr_response

    def iterate_samples_without_qr(
        self, sample_indices: Optional[Tuple[int]] = None, **_
    ) -> Iterator[str]:
        stepsInBetweenSamples = self._step_size
        initialOffset = self._initial_offset
        sample_indices = self._parse_sample_indices(sample_indices)

        for sample_index in sample_indices:
            # self.translation.offset = 0  # <= YW modif
            self.translation.move(
                initialOffset + stepsInBetweenSamples * sample_index, wait=True
            )
            yield f"streamline_sample{sample_index:02d}"

    def select_sample(self, sample_index: int, **qrread_options) -> str:
        for qr_response in self.iterate_samples([sample_index], **qrread_options):
            return qr_response

    def select_sample_without_qr(self, sample_index: int, **_) -> str:
        for qr_response in self.iterate_samples_without_qr([sample_index]):
            return qr_response

    def _parse_sample_indices(
        self, sample_indices: Optional[Tuple[int]] = None
    ) -> Tuple[int]:
        if sample_indices:
            not_allowed = set(sample_indices) - set(self._allowed_sample_indices)
            if not_allowed:
                raise ValueError(
                    f"The following sample indices are not allowed: {not_allowed}"
                )
        else:
            sample_indices = self._allowed_sample_indices
        return sample_indices

    def qr_read(self, **qrread_options) -> str:
        qrread_options.setdefault("timeout", self._qrread_timeout)
        response = self.qrreader.read(**qrread_options)
        if not response:
            raise RuntimeError("Cannot read QR code")
        return response

    def tune_qrreader(self, force=False) -> str:
        if force:
            self.qrreader.autotuning()
            return self.qr_read(timeout=self._qrread_timeout, autoTuningAllowed=False)
        else:
            return self.qr_read(timeout=self._qrread_timeout, autoTuningAllowed=True)

    def safety_checks(self):
        if not self.is_cover_in():
            raise RuntimeError(
                "No cover over the sample holder rack. Please close the cover with the aluminum plate."
            )
        if not self.translation.hw_state == "READY":
            raise Exception(
                "MOTOR NOT READY. Icepap motor driver is not ready. Switch ON the driver, check the cable ..."
            )
        if self.motor_on_state() != "ON":
            raise Exception("MOTOR NOT ON. Switch ON the driver, check the cable ...")
        # check gas pressure ?
        if not self.is_air_pressure_ok():
            raise Exception(
                "AIR PRESSURE FAULT. Check the air pressure needed for the cylinder. Pressure should be between 4 and 6 bars. Display is blue when all is good."
            )

        # check hutch interlock ?

    def motor_on_state(self) -> str:
        from bliss.controllers.motors.icepap.comm import _command

        self.translation.position
        pre_cmd = "%s:" % self.translation.address
        return _command(self.translation.controller._cnx, pre_cmd + "?POWER")

    def is_cylinder_rewinded(self):
        return self.wago.get("DI24V_2_2")

    def in_home_position(self):
        return self._wago.get("DI24V_2_4")

    def is_cylinder_finishing_loading_baguette(self):
        return self.wago.get("DI24V_2_1")

    def is_cover_in(self):
        return not self.wago.get("DI24V_2_3")

    def is_air_pressure_ok(self):
        return self.wago.get("DI24V_2_5")  # This is the festo pressure gas 1 == good

    def cylinder_load_baguette(self, wait=True):
        self.safety_checks()
        self.wago.set("DO24V_1_commutneg_1", 1)
        self.wago.set("DO24V_1_commutneg_2", 0)
        if wait:
            while not self.is_cylinder_finishing_loading_baguette():
                time.sleep(0.1)

    def cylinder_rewind(self, wait=True):
        self.safety_checks()
        self.wago.set("DO24V_1_commutneg_1", 0)
        self.wago.set("DO24V_1_commutneg_2", 1)
        if wait:
            while not self.is_cylinder_rewinded():
                time.sleep(0.1)

    @property
    def vibration_speed(self):
        return self.wago.get("A0_010V_1") * 33.33333

    @vibration_speed.setter
    def vibration_speed(self, speed):
        self._print("SETTING VIBRATION SPEED TO", speed)
        if speed < 0 or speed > 100:
            raise RuntimeError(
                "Speed for the fluidization system out of range (0-100, as in %)"
            )
        # speed in % => 0-10V => resistor bridge => 0-3.3V
        speed = (
            speed / 33.3333
        )  # You change this you kill the DC motor and its controller.
        self.wago.set("A0_010V_1", speed)

    def load_baguette_with_homing(self):
        self._print("Loading new sample holder")
        vibrationBefore = self.vibration_speed
        self.vibration_speed = 0

        try:
            if not self.is_cover_in():
                self._print(
                    "There is no cover over the sample holders rack. Put the Al cover in place."
                )
                return

            if not self.has_remaining_baguettes():
                self._print("There is no baguette")
                return

            if self.translation.position < 18:
                self._print(
                    "The baguette is not unloaded ! Crash avoided (position<18), you are welcome."
                )
                return

            if not self.is_cylinder_rewinded():
                self._print(
                    "The baguette launcher is not rewinded ! Crash avoided, you are welcome."
                )
                self.cylinder_rewind()

            self.translation.velocity = self._maxspeed

            self._print("Checks done")

            self.cylinder_load_baguette(wait=False)
            # self._print("Cylinder loading done")
            self.translation.home(switch=-1)
            time.sleep(0.1)
            # self._print("Homing of border done "+str(self.translation.home_found_dial()) )
            # time.sleep(3)
            self.translation.sync_hard()  # Without We had a bug here on 10/10/22
            self.cylinder_rewind(wait=True)
            self._print("Hard sync done" + str(self.translation.home_found_dial()))
            self.translation.offset = 0
            self.translation.move(0.25)
            # self._print("Moving away from border done")
            # time.sleep(3)
            self.translation.home(switch=-1)
            # self._print("Homing first(?) position done"+str(self.translation.home_found_dial()))
            # time.sleep(3)
            time.sleep(0.1)
            self.translation.sync_hard()  # Without We had a bug here on 10/10/22
            # self._print("Hard sync done")
            self.safe_home_translation()
            self._refine_home_position(
                startposition=7, offsetbefore=-0.5, offsetafter=0.5, velocity=0.5
            )
            self._refine_home_position(
                startposition=7, offsetbefore=-0.11, offsetafter=0.11, velocity=0.05
            )
        finally:
            self.vibration_speed = vibrationBefore

    def safe_home_translation(self):
        """Moves to the first sample and resets the position to zero."""
        self._safe_home_translation()

    def _safe_home_translation(self):
        if self.in_home_position() and self.translation.hw_state.HOME:
            self._print(
                "\n Homing was good and confirmed. "
                + str(self.translation.home_found_dial())
            )
            return

        extraoffset = 0

        for i in range(0, self._maxAttempsToCheckHoming):
            self._print("Searching first home hole ")
            self.translation.offset = 0
            self.translation.move(-i)
            if self.in_home_position():
                self._print("Found correct home hole (first Sample)")
                while self.translation.hw_state.HOME:
                    if extraoffset > 10:
                        raise Exception(
                            "Impossible to home inconsistent between home and first position sensors."
                        )

                    self._print("Getting out of home hole to do a clean homing")
                    extraoffset += 1
                    self.translation.move(-i - 0.05 - 0.05 * extraoffset)

                self._print("Final homing ...")
                self.translation.home(switch=-1)
                time.sleep(0.1)
                self.translation.sync_hard()

                self._print(
                    "\n homing done, double checking "
                    + str(self.translation.home_found_dial())
                )
                # return
                self.translation.move(0)
                # time.sleep(3)
                if not (self.in_home_position() and self.translation.hw_state.HOME):
                    self._safe_home_translation()  # recursing call that should return only when homing is perferct. Probably risky for unknown (yet) problematic cases. If this is a problem replace by a return statement here.
                    return
                else:
                    return

    def _safe_home_translation_one_laser(self):
        for i in range(0, self._maxAttempsToCheckHoming):
            self._print("\n\n\nMoving outside baguette to be sure, offset: " + str(i))
            self.translation.velocity = self._gentlespeed
            self.translation.offset = 0
            self.translation.move(-i - 0.65)
            if self.translation.hw_state.HOME:
                if i == 0:
                    self.translation.offset = 0
                    self.translation.move(
                        0
                    )  # was ok on the first attempt, is still true ?
                    if self.translation.hw_state.HOME:
                        self._print(
                            "\n Homing was good and confimed. "
                            + str(self.translation.home_found_dial())
                        )
                        self.translation.velocity = self._maxspeed
                        return i  # all good
                    else:
                        self.translation.offset = 0
                        self.translation.move(-0.05)
                        self.translation.home(switch=-1)
                        # time.sleep(2)
                        time.sleep(0.1)
                        self.translation.sync_hard()
                        self._print(
                            "\n Homing was thought good but not confimed. "
                            + str(self.translation.home_found_dial())
                        )

                else:
                    self.translation.offset = 0
                    self.translation.move(
                        -i - 0.05
                    )  # there was an alignment error. rehoming one last time.
                    # time.sleep(2)
                    self._print("\nHoming Again to validate the found offset")
                    self.translation.home(switch=-1)
                    # time.sleep(2)
                    time.sleep(0.1)
                    self.translation.sync_hard()
                    self._print(
                        "\n Re Homing done, error was :"
                        + str(self.translation.home_found_dial())
                    )
                    self._safe_home_translation_one_laser()

                self.translation.velocity = self._maxspeed
                return i
        raise Exception(
            "Impossible to home this sample holder in a correct amount of time."
        )

    def _refine_home_position(
        self, startposition=0, offsetbefore=-0.5, offsetafter=0.5, velocity=0.5
    ):

        speedBefore = self.translation.velocity
        center = self._guessThisPositionNoScans(
            startposition, offsetbefore, offsetafter, velocity
        )
        self.translation.velocity = speedBefore

        if center:
            print("Refining to " + str(center))
            self.translation.move(startposition + offsetbefore)
            self.translation.move(center)
            posNOTCACHED = (
                self.translation.controller.read_position(self.translation, cache=False)
                / self.translation.steps_per_unit
                * self.translation.sign
                + self.translation.offset
            )
            if abs(posNOTCACHED - center) > 1e-5:
                print(
                    "BIG PROBLEM HERE, positions should be the same "
                    + str((posNOTCACHED, center))
                )
            else:
                print("Refinement is a succes")
            self.translation.position = startposition
            self.translation.sync_hard()
            time.sleep(0.5)

    def _guessThisPositionNoScans(
        self, startposition=0, offsetbefore=-0.5, offsetafter=0.5, velocity=0.5
    ):

        # speedBefore = self.translation.velocity
        nbpoints = 0

        self.translation.move(startposition + offsetbefore)
        if self.translation.hw_state.HOME:
            print("CANNOT REFINE, Already in the hole.")
            return False
        else:
            self.translation.velocity = velocity
            self.translation.move(startposition + offsetafter, wait=False)
            while (
                not self.translation.hw_state.HOME
                and self.translation._hw_position < startposition + offsetafter
                and self.translation.is_moving
            ):
                posNOTCACHED = (
                    self.translation.controller.read_position(
                        self.translation, cache=False
                    )
                    / self.translation.steps_per_unit
                    * self.translation.sign
                    + self.translation.offset
                )
                if False:
                    print(
                        str(
                            (
                                self.translation._hw_position,
                                self.translation.hw_state.HOME,
                                posNOTCACHED,
                            )
                        )
                    )
                nbpoints += 1
                pass
            risingEdgeApprox = self.translation._hw_position
            risingEdge = self.translation.controller.read_position(
                self.translation, cache=False
            )

            while (
                self.translation.hw_state.HOME
                and self.translation._hw_position < startposition + offsetafter
                and self.translation.is_moving
            ):
                posNOTCACHED = (
                    self.translation.controller.read_position(
                        self.translation, cache=False
                    )
                    / self.translation.steps_per_unit
                    * self.translation.sign
                    + self.translation.offset
                )
                if False:
                    print(
                        str(
                            (
                                self.translation._hw_position,
                                self.translation.hw_state.HOME,
                                posNOTCACHED,
                            )
                        )
                    )
                nbpoints += 1
                pass
            fallingEdgeApprox = self.translation._hw_position
            fallingEdge = self.translation.controller.read_position(
                self.translation, cache=False
            )
        print(
            str(
                (
                    self.translation._hw_position,
                    self.translation.hw_state.HOME,
                    self.translation.controller.read_position(
                        self.translation, cache=False
                    ),
                )
            )
        )
        self.translation.stop()
        centerEncoder = (risingEdge + fallingEdge) / 2
        center = (
            centerEncoder / self.translation.steps_per_unit
        ) * self.translation.sign + self.translation.offset
        centerApprox = (risingEdgeApprox + fallingEdgeApprox) / 2
        print(
            "\nRefinement results : "
            + str(
                (risingEdge, fallingEdge, nbpoints, centerEncoder, centerApprox, center)
            )
        )
        return center

    def has_remaining_baguettes(self) -> bool:
        return self.wago.get("AI010V_1") < 3

    @property
    def number_of_remaining_baguettes(self) -> int:
        zeroOrOneBaguette = 5.76189458906827
        sixtyEightBaguette = 0.25299844355601675
        wagoin = self.wago.get("AI010V_2")
        return int(self.has_remaining_baguettes()) + round(
            (zeroOrOneBaguette - wagoin)
            / ((zeroOrOneBaguette - sixtyEightBaguette) / 67)
        )

    def eject_old_baguette(self):
        self.translation.sync_hard()
        self.translation.offset = 0
        if self.translation.position > 20:
            self.translation.move(self.translation.position + 20)
        else:
            self.translation.move(20)

    def _print(self, *args):
        print(*args)

    def test(self):
        qrs = []
        errors = 0
        while self.has_remaining_baguettes():
            vibrationSpeedBefore = self.vibration_speed
            self.vibration_speed = 0
            self.eject_old_baguette()
            self.load_baguette_with_homing()
            self.vibration_speed = vibrationSpeedBefore
            for qr in self.iterate_samples():
                qrs.append(qr)
                self._print(qr)
                if qr == self.qrreader.QRCODE_NOT_READABLE:
                    self._print(" \n\n\n ERROR QR CODE, not readable ")
                    errors += 1
                    time.sleep(2)

        self.vibration_speed = 0
        self.eject_old_baguette()
        return (qrs, errors)
