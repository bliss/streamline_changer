import time
import socket
import logging
from typing import List, Optional

try:
    from bliss.config.settings import SimpleSetting
except ImportError:

    class SimpleSetting:
        def __init__(self, *_, **kw) -> None:
            self.value = None

        def get(self):
            return self.value

        def set(self, value):
            self.value = int(value)


logger = logging.getLogger(__name__)


class SR700NL20:
    """QR-code reader

    https://www.keyence.fr/products/barcode/barcode-readers/sr-700/models/n-l20/
    """

    QRCODE_NOT_READABLE = "QRCODE_NOT_READABLE"

    def __init__(self, hostname="localhost", port=9004, connect_timeout=3):
        self.__sock = None
        self._connect_timeout = connect_timeout
        self._socket_timeout = min(max(connect_timeout / 5, 0.5), 1)
        self._hostname = hostname
        self._port = port
        self.lastQrCode = ""
        self.lastQrCodeTime = float("nan")
        self.goodRoi = (272, 84, 505, 315)
        self.overrideAllROI()
        self.allowAutotuningOnQRCodesOnly()
        self.redisSettingLastAutotuneBank = SimpleSetting(
            hostname + "_last_autotune_bank", write_type_conversion=int
        )
        self.lastAutotunedBank = self.redisSettingLastAutotuneBank.get()
        if self.lastAutotunedBank is None:
            self.redisSettingLastAutotuneBank.set(3)
            self.lastAutotunedBank = self.redisSettingLastAutotuneBank.get()

    def __del__(self):
        self.disconnect()

    def __repr__(self) -> str:
        return f"{type(self).__name__}({repr(self._hostname)}, {repr(self._port)})"

    @property
    def _socket(self):
        if self.__sock is None:
            self.connect()
        return self.__sock

    def connect(self):
        """
        :raises ConnectionError:
        """
        if self.__sock is not None:
            return
        sock = None
        last_exception = None
        nretries = max(int(self._connect_timeout / self._socket_timeout + 0.5), 1)
        for _ in range(nretries):
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.settimeout(self._socket_timeout)
                sock.connect((self._hostname, self._port))
                break
            except Exception as e:
                last_exception = e
                sock = None
        if sock is None:
            raise ConnectionError(f"{self}: cannot connect") from last_exception
        self.__sock = sock
        self.clearBuffer()

    def reconnect(self):
        self.disconnect()
        self.connect()

    def disconnect(self):
        try:
            if self.__sock is not None:
                self.__sock.close()
        except Exception:
            pass

    def sendCmd(self, command, timeout=3) -> List[str]:
        data = bytes(command + "\r", encoding="utf-8")
        t0 = time.time()

        def check_timeout(exception=None):
            if (time.time() - t0) > timeout:
                try:
                    if command.startswith("LON"):
                        self.stopRead()
                finally:
                    raise TimeoutError(
                        f"{self}: cannot send command {command}"
                    ) from exception

        while True:
            # Do not capture connection errors
            sock = self._socket

            # Send the request
            try:
                sock.sendall(data)
            except Exception as e:
                check_timeout(e)
                time.sleep(0.1)
                self.reconnect()
                continue

            # Get the response
            try:
                answer = sock.recv(1500)
                res = answer.decode(encoding="utf-8").rstrip().split(",")
                return res
            except Exception as e:
                check_timeout(e)
                time.sleep(0.1)

    def clearBuffer(self) -> None:
        self.sendCmd("%BCLR")

    @property
    def version(self) -> List[str]:
        res = self.sendCmd("%KEYENCE")
        return res[2:]

    @property
    def errorStatus(self) -> List[str]:
        res = self.sendCmd("%ERRSTAT")
        return res[2:]

    @property
    def busy(self) -> List[str]:
        res = self.sendCmd("%BUSYSTAT")
        return res[2:]

    def read(self, bank="00", timeout=1, autoTuningAllowed=True) -> Optional[str]:
        if bank == "00":
            res = self.sendCmd("LON", timeout)
        else:
            res = self.sendCmd("LON," + bank, timeout)
        if not res:
            return None

        response, _, banknr = res[0].rpartition(":")
        if not banknr.isdigit():
            response = res[0]
        else:
            print("Successfully read on bank:" + str(banknr))

        if response == "ERROR":
            response = self.QRCODE_NOT_READABLE

        if response == self.QRCODE_NOT_READABLE and autoTuningAllowed:
            # Attempting one autotuning on the next not used autotuned bank
            self.autotuning()
            return self.read(timeout=timeout, autoTuningAllowed=False)

        self.lastQrCode = response
        self.lastQrCodeTime = time.time()
        return self.lastQrCode

    def stopRead(self) -> List[str]:
        res = self.sendCmd("LOFF", timeout=1)
        return res

    def readBadQrCodes(self):
        # first read the bank whiched worked last time.

        return

    def laserPointerOn(self) -> List[str]:
        res = self.sendCmd("AMON")
        return res

    def laserPointerOff(self) -> List[str]:
        res = self.sendCmd("AMOFF")
        return res

    def allowAutotuningOnQRCodesOnly(self):
        cmd = "WB,820,1"
        _ = self.sendCmd(cmd)
        self.save()

    def setROI(self, bank, corner1, corner2, corner3, corner4, save=True):
        # corner 1 and 2 must be EVEN
        # corner 3 and 4 must be ODD
        bank = str(bank).zfill(2)
        corner1 = str(corner1).zfill(3)
        corner2 = str(corner2).zfill(3)
        corner3 = str(corner3).zfill(3)
        corner4 = str(corner4).zfill(3)

        print(
            "Setting QrCode reader ROI on bank {} with value {} {} {} {}".format(
                bank, corner1, corner2, corner3, corner4
            )
        )
        cmd = "WB,{}604,0{}0{}0{}0{}".format(bank, corner1, corner2, corner3, corner4)
        res = self.sendCmd(cmd)
        if save:
            self.save()
        return res

    def overrideAllROI(self):
        for bank in range(1, 11):
            self.setROI(
                bank,
                self.goodRoi[0],
                self.goodRoi[1],
                self.goodRoi[2],
                self.goodRoi[3],
                save=False,
            )
        self.save()  # Saving is long.

    def save(self):
        res = self.sendCmd("SAVE")
        return res

    def autotuning(self):
        if self.lastAutotunedBank + 1 > 10:
            bankToTry = 3
        else:
            bankToTry = self.lastAutotunedBank + 1
        self.tuning(bank=bankToTry)

    def tuning(self, bank=3) -> List[str]:
        if bank == 1 or bank == 2:
            print("Tuning on bank or 2 is not allowed, 3 to 10 OK.")
            return

        res = self.sendCmd("TUNE" + str(bank).zfill(2))

        exitingCodes = ("Tuning SUCCEEDED", "Tuning FAILED")
        # On attends la fin du tuning:
        try:
            res = self.read(bank=str(bank).zfill(2), timeout=1, autoTuningAllowed=False)
        except Exception:
            pass

        waitForTuning = True
        while waitForTuning:
            try:
                res = self.read(
                    bank=str(bank).zfill(2), timeout=1, autoTuningAllowed=False
                )
                for stopcode in exitingCodes:
                    if res.find(stopcode):
                        waitForTuning = False
                        break
                time.sleep(0.5)
            except Exception:
                pass

        if res.find("SUCCEEDED"):
            self.redisSettingLastAutotuneBank.set(bank)
            self.lastAutotunedBank = bank

        self.setROI(
            bank,
            self.goodRoi[0],
            self.goodRoi[1],
            self.goodRoi[2],
            self.goodRoi[3],
            save=True,
        )
        return self.read(bank=str(bank).zfill(2), timeout=1, autoTuningAllowed=False)

    def stoptuning(self) -> List[str]:
        res = self.sendCmd("TQUIT")
        return res

    def printBankParameters(self) -> List[str]:
        paramlist = (
            list(range(100, 105))
            + list(range(400, 404))
            + list(range(600, 109))
            + list((500, 611))
        )
        res = ""
        for bank in range(1, 11):
            line = "bank: " + str(bank).zfill(2) + " "
            for param in paramlist:
                res = self.sendCmd(
                    "RB,{}{}".format(str(bank).zfill(2), str(param).zfill(3))
                )[2]
                line += " ;  " + str(param) + ":" + res
            print(line)
            res += "\n" + line
        return res


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Test QR-code reader")
    parser.add_argument(
        "--reader",
        choices=["none", "local", "basf", "bm23"],
        default="none",
    )
    parser.add_argument(
        "--host",
        type=str,
        default="localhost",
    )
    parser.add_argument(
        "--port",
        type=int,
        default=9004,
    )
    args = parser.parse_args()
    if args.reader == "none":
        host = args.host
        port = args.port
    elif args.reader == "basf":
        host = "172.24.167.201"
        port = 9004
    elif args.reader == "bm23":
        host = "172.24.167.201"
        port = 9004
    else:
        host = "127.20.0.1"
        port = 9004
    reader = SR700NL20(host, port)
    print("Version:", reader.version)
    print("QR code:", reader.read())
