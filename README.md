# Streamline changer

Streamline sample changer code, with QR code reader and BLISS controller.

## Getting started

When configured in Bliss as shown below, you can use the sample changer as follows

```
MY_SESSION [1]: for qrcode in streamline_sc.iterate_samples():
                    print(qrcode)
MY_SESSION [2]: for qrcode in streamline_sc.iterate_samples([1, 3, 6]):
                    print(qrcode)
MY_SESSION [3]: qrcode = streamline_sc.select_sample(5)
```

The sample indices are integers between 0 and 15.

The methods `iterate_samples_without_qr` and `select_sample_without_qr` achieve the
same without reading the QR code. Instead the string `"streamline_sample{idx}"` is
returned with `idx` the sample index.

## Testing

Run test suite

```python
python -m pip install -e .[test]
pytest
```

Manual QR testing

```bash
python -m pip install -e .
python -m streamline_changer.qrreader
```

## Beamline configuration

Streamline sample changer

```yaml
# /users/blissadm/local/beamline_configuration/EH/streamline_sc.yml

name: streamline_sc
plugin: bliss
description: Streamline sample changer
package: streamline_changer.bliss
class: BlissSampleChanger
sc_axis: $streamline_translation
sc_wago: $streamline_wago
qrreader_host: 160.103.51.65
#qrreader_port: 9004
#wait_to_stop_before_qr_code_reading: true
#step_size: 1
#initial_offset: 0
#delay_for_qr_code_reading: 0.03
#number_of_samples: 16
```

Sample changer translation motor

```yaml
# /users/blissadm/local/beamline_configuration/EH/motion/iceid314.yml

controller:
  class: IcePAP
  host: iceid314
  name: iceid314
  switches:
  - name: iceid314_switch
  axes:
  - name: streamline_translation
    address: 2
    steps_per_unit: 126.8
    sign: -1
    velocity: 55.2
    backlash: 0.0
    acceleration: 1380
```

Sample changer wago box

```yaml
# /users/blissadm/local/beamline_configuration/EH/wagos/streamline_wago.yml

name: streamline_wago
plugin: bliss
description: Wago Box for Streamline robot
module: wago.wago
class: Wago
modbustcp:
    url: wagoses1:502 
mapping:
  - type: 750-478
    logical_names: AI010V_1, AI010V_2 
  - type: 750-474
    logical_names: AI420mA_1, AI420mA_2 
  - type: 750-436
    logical_names: DI24V_1_commutneg_1, DI24V_1_commutneg_2, DI24V_1_commutneg_3, DI24V_1_commutneg_4, DI24V_1_commutneg_5, DI24V_1_commutneg_6, DI24V_1_commutneg_7, DI24V_1_commutneg_8
  - type: 750-430
    logical_names: DI24V_2_1, DI24V_2_2, DI24V_2_3, DI24V_2_4, DI24V_2_5, DI24V_2_6, DI24V_2_7, DI24V_2_8
  - type: 750-516
    logical_names: DO24V_1_commutneg_1, DO24V_1_commutneg_2, DO24V_1_commutneg_3, DO24V_1_commutneg_4, DO24V_1_commutneg_5, DO24V_1_commutneg_6, DO24V_1_commutneg_7, DO24V_1_commutneg_8 
  - type: 750-504
    logical_names: DO24V_2_1, DO24V_2_2, DO24V_2_3, DO24V_2_4, DO24V_2_5, DO24V_2_6, DO24V_2_7, DO24V_2_8 
  - type: 750-550
    logical_names: A0_010V_1, A0_010V_2
ignore_missing: True
counter_names: DI24V_1_commutneg_1, DI24V_1_commutneg_2, DI24V_1_commutneg_3, DI24V_1_commutneg_4, DI24V_1_commutneg_5, DI24V_1_commutneg_6, DI24V_1_commutneg_7, DI24V_1_commutneg_8, DI24V_2_1, DI24V_2_2, DI24V_2_3, DI24V_2_4, DI24V_2_5, DI24V_2_6, DI24V_2_7, DI24V_2_8, AI010V_1, AI010V_2 , AI420mA_1, AI420mA_2, AI010V_1, AI010V_2 
```

## SES lab

```python
from streamline_changer.qrreader import SR700NL20

qrreader = SR700NL20("172.24.167.201" ,9004)
```

In step unit Icepap file (not to be used on beamline, for tests only)

```yaml
 - name: translation
   address: 1
   steps_per_unit: 1  #### in steps unit
   sign: -1
   velocity: 7000
   backlash: 0.0
   acceleration: 175000
```

Icepap file to be used on beamline (in sample units).

```yaml
 - name: translation
   address: 1
   steps_per_unit: 126.8
   sign: -1
   velocity: 55.20
   backlash: 0.0
   acceleration: 1380
```

Distance between to samples

```python
 BASF [140]: 1912/15
  Out [140]: 127.46666666666667  # With homing (to be redone)
```
By eyes: 126.8 steps between sample positions
